package time;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TimeTest {

	
	
	@Test
	public void testGetTotalMilliseconds() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		//int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test 
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:1000");
		assertFalse("Invalid number of milliseconds", totalMilliseconds == 1000);
	}
	
	
	
	
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		
		// If the expected value doesnt match the actual value, then failed
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
		
		//fail("Not yet implemented");
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		
		//int totalSeconds = Time.getTotalSeconds("01:01:0a");
		fail("The time provided is not valid");
		
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		assertTrue("The time provided does not match the result", totalSeconds == 3719);
		
	}
	
	// as long as the time format is within the range, test passes
	// or can expect an exception
	@Test (expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertFalse("The time provided does not match the result", totalSeconds == 3720);
		
	}
	
//	@Test (expected = NumberFormatException.class)
//	public void testGetTotalSecondsBoundaryOut() {
//		
//		int totalSeconds = Time.getTotalSeconds("01:01:60");
//		fail("The time provided does not match the result");
//		
//	}

}
